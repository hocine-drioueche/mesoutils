
<!-- README.md is generated from README.Rmd. Please edit that file -->

# mesoutils

<!-- badges: start -->
<!-- badges: end -->

The goal of mesoutils is to …

## Installation

You can install the development version of mesoutils like so:

``` r
# FILL THIS IN! HOW CAN PEOPLE INSTALL YOUR DEV PACKAGE?
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(mesoutils)
## basic example code
```

Une fonction qui permettra d’avoir des informations sur un jeu de :
dimensions et nom des colonnes

``` r
get_info_data(data = iris)
#> $dimension
#> [1] 150   5
#> 
#> $names
#> [1] "Sepal.Length" "Sepal.Width"  "Petal.Length" "Petal.Width"  "Species"
```

Une fonction qui permettra d’obtenir une table avec la moyenne de toutes
les variables numériques

``` r
get_mean_data(data = iris)
#>   Sepal.Length Sepal.Width Petal.Length Petal.Width
#> 1     5.843333    3.057333        3.758    1.199333
```
